import { Tweet } from './tweet';
export interface Mention {
  tweet: Tweet;
  is_vote: boolean;
  YMD: string;
  YWeek: string;
}
