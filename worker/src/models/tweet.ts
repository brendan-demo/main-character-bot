export interface Hashtags {
  text: string;
  indices?: any[];
}

export interface UserMention {
  screen_name: string;
  name: string;
  id: number;
  id_str: string;
  indices: any[];
}

export interface URL {
  url: string;
  expanded_url: string;
  display_url: string;
  indices: any[];
}

export interface TweetEntities {
  hashtags: Hashtags[];
  symbols: any[];
  user_mentions: UserMention[];
  urls: URL[];
}

export interface Tweet {
  created_at: string;
  id: number;
  text: string;
  entities: TweetEntities;
  source?: string;
  in_reply_to_status_id?: number;
  in_reply_to_user_id?: number;
  in_reply_to_screen_name?: string;
}
