// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
import { AccessTokenOptions } from 'twitter';

const consumer_key: string = process.env.CONSUMER_KEY || '';
const consumer_secret: string = process.env.CONSUMER_SECRET || '';
const access_token_key: string = process.env.ACCESS_TOKEN_KEY || '';
const access_token_secret: string = process.env.ACCESS_TOKEN_SECRET || '';

const TwitterConfig: AccessTokenOptions = {
  consumer_key,
  consumer_secret,
  access_token_key,
  access_token_secret,
};

export { TwitterConfig };
