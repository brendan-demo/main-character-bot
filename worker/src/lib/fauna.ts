/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
const faunadb = require('faunadb'),
  q = faunadb.query;

const client = new faunadb.Client({ secret: process.env.FAUNADB_SECRET });

const createTest = client.query(
  q.Create(q.Collection('test'), { data: { testField: 'testValue' } }),
);

export async function logIt(message: string): Promise<any> {
  const now = new Date();
  return client.query(
    q.Create(q.Collection('logs'), {
      data: {
        message: message,
        timestamp: now.toISOString(),
      },
    }),
  );
}

export { createTest, q, faunadb, client };
