import Twitter from 'twitter';
import os from 'os';

import { TwitterConfig } from './lib/TwitterConfig';
import { updateMentions } from './services/mentionsService';
import { logIt } from './lib/fauna';

const client = new Twitter(TwitterConfig);

const startMsg = `Startup on ${os.hostname()}`;
console.log(startMsg);
logIt(startMsg);

updateMentions(client);

// createTest
//   .then(function (response: any) {
//     console.log(response.ref); // Would log the ref to console.
//   })
//   .catch((err: any) => console.error(err));
