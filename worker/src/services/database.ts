import { client, q } from '../lib/fauna';
import { Mention } from '../models/mention';

export async function addMention(mention: Mention): Promise<any> {
  return client.query(
    q.Create(q.Collection('mentions'), { data: { ...mention } }),
  );
}
