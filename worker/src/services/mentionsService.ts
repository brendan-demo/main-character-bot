import Twitter from 'twitter';
import { Tweet } from '../models/tweet';
import { addMention } from './database';
import { getTweetFromRaw } from './tweetReader';

export async function updateMentions(client: Twitter): Promise<any> {
  return new Promise((resolve, reject) => {
    client.get(
      'statuses/mentions_timeline',
      {},
      function (error, tweets, response) {
        if (!error) {
          tweets.forEach((tweet: any) => {
            const thisTweet: Tweet = getTweetFromRaw(tweet);
            addMention({
              tweet: thisTweet,
              is_vote: false,
              YMD: '',
              YWeek: '',
            })
              .then((val) => {
                console.log(val);
                resolve('ok');
              })
              .catch((err) => {
                console.error('Error writing to DB');
                console.error(err);
              });
          });
        } else {
          console.error(error);
          reject(error);
        }
      },
    );
  });
}
