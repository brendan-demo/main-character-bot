import { pick } from 'lodash';
import { Tweet } from '../models/tweet';

export function getTweetFromRaw(rawtweet: any): Tweet {
  const thisTweet: Tweet = pick(rawtweet, [
    'created_at',
    'id',
    'text',
    'entities',
    'source',
    'in_reply_to_status_id',
    'in_reply_to_user_id',
    'in_reply_to_screen_name',
  ]);
  return thisTweet;
}
