const twit = require('./lib/twitter');
const db = require('./lib/db');

exports.handler = async (event, context) => {
  switch (event.httpMethod) {
    case 'GET':
      try {
        const fullTweet = event.queryStringParameters.fullTweet;
        const mentions = await twit.getMentions(fullTweet).catch((err) => {
          return {
            statusCode: 500,
            body: { error: err.toString(), function: 'twit.getMentions' },
          };
        });
        await db.logIt('Mention check', 'mentioncheck');

        mentions.forEach(async (tweet) => {
          await db.addTweet(tweet);
        });

        return {
          statusCode: 200,
          body: JSON.stringify(mentions),
        };
      } catch (error) {
        return {
          statusCode: 500,
          body: { error: error.toString(), function: 'checkTwitter.handler' },
        };
      }
    case 'DELETE':
      try {
        const del = await db.deleteAllTweets();
        return {
          statusCode: 200,
          body: JSON.stringify({ deleted: 'all' }),
        };
      } catch (error) {
        return { statusCode: 500, body: error.toString() };
      }
  }
};
