/* Import faunaDB sdk */
const faunadb = require('faunadb');

const q = faunadb.query;
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
});

exports.logIt = async (message, type = 'general') => {
  const now = new Date();
  return client.query(
    q.Create(q.Collection('logs'), {
      data: {
        type,
        message: message,
        timestamp: now.toISOString(),
      },
    })
  );
};

// tweet: Tweet;
// is_vote: boolean;
// YMD: string;
// YWeek: string;
exports.addTweet = async (tweet) => {
  const now = new Date();
  return client.query(
    q.If(
      q.Not(q.Exists(q.Match(q.Index('tweetId'), tweet.id_str))),
      q.Create(q.Ref(q.Collection('mentions'), tweet.id_str), {
        data: {
          tweet,
          is_vote: false,
          YMD: '',
          YWeek: '',
          timestamp: now.toISOString(),
        },
      }),
      q.Ref(q.Collection('mentions'), tweet.id_str)
    )
  );
};

exports.deleteAllTweets = async () => {
  return client.query(
    q.Map(
      q.Paginate(q.Documents(q.Collection('mentions')), { size: 9999 }),
      q.Lambda(['ref'], q.Delete(q.Var('ref')))
    )
  );
};

// exports.readAll = async (event, context) => {
//   console.log('Function `read-all` invoked');
//   return (
//     client
//       //.query(q.Paginate(q.Match(q.Ref('indexes/tweetId'))))
//       //.query(q.Paginate(q.Collections()))
//       .query(q.Match(q.Index('tweetId'), 1369127585146237000))
//       .then((response) => {
//         const itemRefs = response.data;
//         // create new query out of item refs. http://bit.ly/2LG3MLg
//         const getAllItemsDataQuery = itemRefs.map((ref) => {
//           return q.Get(ref);
//         });
//         // then query the refs
//         return client.query(getAllItemsDataQuery).then((ret) => {
//           return {
//             statusCode: 200,
//             body: JSON.stringify(ret),
//           };
//         });
//       })
//       .catch((error) => {
//         console.log('error', error);
//         return {
//           statusCode: 400,
//           body: JSON.stringify(error),
//         };
//       })
//   );
// };
