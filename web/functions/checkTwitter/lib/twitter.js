const Twitter = require('twitter');
const pick = require('lodash/pick');

const client = new Twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET,
});

async function getMentions(full) {
  return new Promise(async (resolve, reject) => {
    try {
      const resp = await client.get('statuses/mentions_timeline', {});
      let body = [];

      if (full) {
        body = resp;
      } else {
        resp.forEach((rawTweet) => {
          body.push(simpleTweetFromRaw(rawTweet));
        });
      }

      resolve(body);
    } catch (error) {
      console.error(error);
      reject(error);
    }
  });
}

function simpleTweetFromRaw(rawTweet) {
  let tweet = pick(rawTweet, [
    'created_at',
    'id',
    'id_str',
    'text',
    'truncated',
    'entities',
    'in_reply_to_status_id',
    'in_reply_to_status_id_str',
    'in_reply_to_user_id',
    'in_reply_to_user_id_str',
    'in_reply_to_screen_name',
    'retweet_count',
    'favorite_count',
    'favorited',
    'retweeted',
  ]);

  let user = pick(rawTweet.user, [
    'id',
    'id_str',
    'name',
    'screen_name',
    'url',
    'follwers_count',
    'verified',
    'profile_image_url',
    'profile_image_url_https',
    'following',
    'follow_request_sent',
  ]);

  tweet.user = user;

  return tweet;
}

module.exports = { getMentions };
