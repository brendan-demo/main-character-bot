// Docs on event and context https://www.netlify.com/docs/functions/#the-handler-method
exports.handler = async (event, context) => {
  //console.log(process.env.FAUNADB_SERVER_SECRET);
  switch (event.httpMethod) {
    case 'GET':
      return require('./read-all').handler(event, context);
  }

  return {
    statusCode: 500,
    body: 'unrecognized HTTP Method, must be one of GET/POST/PUT/DELETE',
  };
};
