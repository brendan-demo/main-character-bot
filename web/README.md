# Web Frontend

Deploye to Netlify

## Development

1. `yarn`
1. `yarn dev`
1. Go to http://localhost:3000

## Netlify Stack Development

1. Run `yarn`
1. Install the netlify CLI globally with `npm install -g netlify-cli`
1. Run `netlify dev`
1. Go to http://localhost:8888
